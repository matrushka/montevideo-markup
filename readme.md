#About this

This repository contains development history of *css* styles build over *sass* files. It uses static html templates that also define the structure-hierachy of Montevideo's front application markup.

Here's a list of what you can find here:

* Sass source files
* CSS compiled files
* HTML markup with SMACSS classes
* Image resources
* Font resources
* Some basic JS files (might not be the implemented ones)