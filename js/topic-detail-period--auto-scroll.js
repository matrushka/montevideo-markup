jQuery( document ).ready(function($) {
  var $rubricLabels = $('.topic-rubrics-graph__label');
  var $topicPeriodSlider = $('.topic-period-blocks');
  var $topicPeriodMenu = $('.topic-period-menu');

  var setFirstBlokInSlider = function($slider, $menu) {
    $slider.removeClass(function (index, className) {
      return (className.match (/(^|\s)show-\S+/g) || []).join(' ');
    });
    $slider.addClass('show-0');
    $menu.find('.topic-period-menu__item').removeClass('is-selected').first().addClass('is-selected');
  }

  var scrollTo = function($element) {
    var elementOffset = $element.offset();
    $("html, body").animate({ scrollTop: elementOffset.top - 50 }, 600);
  }

  var showRubricDetails = function(ev) {
    ev.preventDefault();
    // Set elements from target
    var $label = $(ev.currentTarget);
    var $button = $('.' + $label.data('target'));

    // Scroll To card
    scrollTo($button);

    // Reset Slider
    setFirstBlokInSlider($topicPeriodSlider, $topicPeriodMenu);

    // Expand card
    $button.addClass('is-expanded');
    $button.removeClass('is-collapsed');
    var $card = $button.parents('.topic-period-data-card');
    $card.addClass('is-expanded');
    $card.removeClass('is-collapsed');
  }

  // Append function to menu
  $rubricLabels.click(showRubricDetails);
});