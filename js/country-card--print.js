jQuery( document ).ready(function($) {

  var print = function(ev) {
    ev.preventDefault();
    var $printButton = $(ev.currentTarget);
    window.print();
  }

  $('.country-card-menu__link--print').click(print);
});