jQuery( document ).ready(function($) {
  var $topicPeriodMenu = $(document).find('.topic-period-menu');
  var $topicPeriodSlider = $(document).find('.topic-period-blocks')
  
  var clearIndicator = function($menu) {
    $menu.find('.topic-period-menu__item').removeClass('is-selected');
  }

  var clearSlider = function($slider) {
    $slider.removeClass(function (index, className) {
      return (className.match (/(^|\s)show-\S+/g) || []).join(' ');
    });
  }

  var changeTab = function(ev) {
    ev.preventDefault();
    
    clearIndicator($topicPeriodMenu);
    clearSlider($topicPeriodSlider);

    var $link = $(ev.currentTarget);
    $link.parents('.topic-period-menu__item').addClass('is-selected');
    var newShowClass = 'show-' + $link.data('show');
    $topicPeriodSlider.addClass(newShowClass);
  }

  // Append function to menu
  if($topicPeriodMenu.length > 0) {
    var $links = $topicPeriodMenu.find('.topic-period-menu__link');
    $links.click(changeTab);
  }
});