jQuery( document ).ready(function($) {
  var $infoPageMenu = $(document).find('.info-page__menu');
  var $infoPageSlider = $(document).find('.info-page__blocks')
  
  var clearIndicator = function($menu) {
    $menu.find('.info-page__menu-item').removeClass('is-selected');
  }

  var clearSlider = function($slider) {
    $slider.removeClass(function (index, className) {
      return (className.match (/(^|\s)show-\S+/g) || []).join(' ');
    });
  }

  var changeTab = function(ev) {
    ev.preventDefault();
    
    clearIndicator($infoPageMenu);
    clearSlider($infoPageSlider);

    var $link = $(ev.currentTarget);
    $link.parents('.info-page__menu-item').addClass('is-selected');
    var newShowClass = 'show-' + $link.data('show');
    $infoPageSlider.addClass(newShowClass);
  }

  // Append function to menu
  if($infoPageMenu.length > 0) {
    var $links = $infoPageMenu.find('.info-page__menu-link');
    $links.click(changeTab);
  }
});