jQuery( document ).ready(function($) {
  var $form = $(document).find('.regional-comparison-toolbar .regional-comparison-toolbar__form');
  
  if($form.length > 0) {

    // Compare function for sort() method
    var compareFunction = function(a, b) {
      if( $(a.element).data('sortable') == false ) return -1;
      a = a.text.toLowerCase();
      b = b.text.toLowerCase();
      if (a > b) {
        return 1;
      } else if (a < b) {
        return -1;
      }
      return 0;
    }

    var $selects = $form.find('select');
    $selects.select2({
      minimumResultsForSearch: 1,
      sorter: function(data) {
        // Chambonada warning (HACK): In safari is required to proper sorting do a pre-call
        data.sort(compareFunction);
        // Iterate over data to find groups and Sort children
        $.each(data, function( index, value ) {
          if(typeof value.children !== "undefined")
            value.children.sort(compareFunction);
        });
        // Sort data (parents) using lowercase comparison
        return data.sort(compareFunction);
      },
    });
  }
});
