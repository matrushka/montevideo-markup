jQuery( document ).ready(function($) {
  var $topicPeriodCards = $('.topic-period-data-card');
  var $sourceTables = $('.source-table__wrapper')

  var toggleState = function(ev) {
    ev.preventDefault();
    var $button = $(ev.currentTarget).find('.topic-period-data-card__collapsible-control');
    $button.toggleClass('is-expanded');
    $button.toggleClass('is-collapsed');
    var $card = $button.parents('.topic-period-data-card');
    $card.toggleClass('is-expanded');
    $card.toggleClass('is-collapsed');
  }

  var toggleTableState = function(ev) {
    ev.preventDefault();
    var $button = $(ev.currentTarget);
    var $tableWrapper = $button.parents('.source-table__wrapper');
    $tableWrapper.toggleClass('is-expanded');
    $tableWrapper.toggleClass('is-collapsed');
  }

  // Append function to menu
  if($topicPeriodCards.length > 0) {
    // var $buttons = $topicPeriodCards.find('.topic-period-data-card__collapsible-control');
    var $buttons = $topicPeriodCards.find('.topic-period-data-card__heading');
    $buttons.click(toggleState);
  }

  // Append function to menu
  if($sourceTables.length > 0) {
    var $buttons = $sourceTables.find('.source-table__switch');
    $buttons.click(toggleTableState);
  }
});