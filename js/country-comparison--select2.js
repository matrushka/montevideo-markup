jQuery( document ).ready(function($) {
  var $form = $(document).find('.country-comparison-menu .country-comparison-menu__form');
  
  if($form.length > 0) {

    // Compare function for sort() method
    var compareFunction = function(a, b) {
      a = a.text.toLowerCase();
      b = b.text.toLowerCase();
      if (a > b) {
          return 1;
      } else if (a < b) {
          return -1;
      }
      return 0;
    }

    var $selects = $form.find('select');
    $selects.select2({
      minimumResultsForSearch: 10,
      sorter: function(data) {
        // Sort data using lowercase comparison
        return data.sort(compareFunction);
      },
    });
  }
});