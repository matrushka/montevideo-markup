// jQuery( document ).ready(function($) {
//   setComparisonTableZebra();
// });

function setComparisonTableZebra() {
  var $rows = $(document).find('.country-comparison-table__row');
  
  if($rows.length > 0) {
    // Filter Rows
    $rows.filter(':odd').addClass('is-odd');
    $rows.filter(':even').addClass('is-even');

    // Filter Columns
    var $columns = $rows.find('.country-comparison-table__column');
    $columns.filter(':odd').addClass('is-odd');
    $columns.filter(':even').addClass('is-even');
    

    // Last
    // var $last_columns = $rows.find('.country-comparison-table__column:last-child');
    // $last_columns.addClass('is-hidden');
  }
}
