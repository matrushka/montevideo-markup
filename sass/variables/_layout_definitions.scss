///////////////////////////////////////////////
//  Element and components size definitions  //
///////////////////////////////////////////////

// Viewport Width Sizes
$vw_xs: 576px;
$vw_sm: 720px;
$vw_md: 940px;
$vw_lg: 1024px;
$vw_xl: 1366px;

// Viewport Height Sizes
$vh_xs: 480px;
$vh_sm: 480px;
$vh_md: 640px;
$vh_lg: 768px;
$vh_xl: 980px;

/* Screen conditions
  -Tiny: (max-width: $vw_xs), (max-height: $vh_xs); // OR
  -Phone: (max-width: $vw_sm), (max-height: $vh_sm); // OR
  -Tablet: (max-width: $vw_md), (max-height: $vh_md); // OR
  -Desktop: (min-width: $vw_lg) and (min-height: $vh_lg); // AND
  -Larger: (min-width: $vw_xl) and (min-height: $vh_xl); // AND
*/

@mixin respondTo($media) {
  @if $media == "tiny" {
    @media only screen and (max-width: $vw_xs), (max-height: $vh_xs) { @content; }
  } @else if $media == "phone" {
    @media only screen and (max-width: $vw_sm), (max-height: $vh_sm) { @content; }
  } @else if $media == "tablet" {
    @media only screen and (max-width: $vw_md), (max-height: $vh_md) { @content; }
  } @else if $media == "desktop" {
    @media only screen and (min-width: $vw_lg) and (min-height: $vh_lg) { @content; }
  } @else if $media == "larger" {
    @media only screen and (min-width: $vw_xl) and (min-height: $vh_xl) { @content; }
  }
}

////////////////////////
// :: Prefix Sizes :: //
// _xs - Extra Small  //
// _sm - Small        //
// _md - Medium       //
// _lg - Large        //
// _xl - Extra Large  //
////////////////////////

// Layout Limits
$content__max-width--md: 1024px;
$content__min-width--md: 768px;

// Top Background
$top-background__height--sm: 300px;
$top-background__height--md: 800px;

// header
$header__height--md: 130px;

// Logo
$logo__width--md: 140px;
$logo__width--sm: 100px;

// Main Menu
$main-menu__height--md: 40px;

// Lang Bar
$lang-bar__width--md: 50px;
$lang-bar__height--md: 50px;

// Download Summary
$download-summary__width--md: 190px;
$download-summary__height--md: 50px;

// Regional Comparison Toolbar
$regional-comparison-toolbar__height--sm: 120px;
$regional-comparison-toolbar__height--md: 100px;

// Indicators
$indicators__height--md: 40px;

// Indicators
$topic-rubrics-data__height--md: 40px;

// Form
$form-control__height--md: 30px;

// Detail page
$info-page__heading-height--md: 120px;

// Footer
$footer__height--md: 100px;

// Footer-Tag
$footer-tag__height--md: 20px;
